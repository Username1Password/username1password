<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
    <head>
      <title>
        <xsl:value-of select="TEI/teiHeader/fileDesc/titleStmt/title" />
      </title>
    </head>
    <body>
      <div style="text-align: center">
        <div>
          <img alt="JainWeb" src="https://username1password.solid.community/public/images/jainweb.png" style="width: 320px" />
        </div>
        <h1>
          Index 1 (.odd→.xml+.xsl→.html)
        </h1>
      </div>
      <br />
      <div>
        <xsl:value-of select="TEI/text/body/p-n1/seg-n1" />
      </div>
      <div>
        <xsl:value-of select="TEI/text/body/p-n1/seg-n2" />
      </div>
      <div>
        <xsl:value-of select="TEI/text/body/p-n1/seg-n3" />
      </div>
      <div>
        <xsl:value-of select="TEI/text/body/p-n1/seg-n4" />
      </div>
      <br />
      <div>
        <xsl:value-of select="TEI/text/body/u/seg-n1" />
      </div>
      <div>
        <xsl:value-of select="TEI/text/body/u/seg-n2" />
      </div>
      <br />
      <div>
        <xsl:value-of select="TEI/text/body/p-n2/seg-n1" />
      </div>
      <div>
        <xsl:value-of select="TEI/text/body/p-n2/seg-n2" />
      </div>
      <br />
      <div style="text-align: justify">
        <xsl:value-of select="TEI/text/body/p-n3" />
      </div>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
