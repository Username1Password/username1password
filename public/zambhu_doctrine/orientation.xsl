<?xml version="1.0" encoding="utf-8"?>

<html xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xsl:version="1.0">
  <head>


      <!-- [title] -->
    <title>
      <xsl:value-of select="TEI/teiHeader/fileDesc/titleStmt/title" />
    </title>






















      <!-- [links] -->
    <link>
      <xsl:attribute name="href">
        <xsl:value-of select="TEI/teiHeader/xenoData/ref/@target" />
      </xsl:attribute>
      <xsl:attribute name="rel">
        <xsl:value-of select="TEI/teiHeader/xenoData/ref/desc" />
      </xsl:attribute>
    </link>

  </head>
  <body>
      <!-- [centering div] -->
    <div>
      <xsl:attribute name="class">
        <xsl:value-of select="TEI/text/figure/@style" />
      </xsl:attribute>
        <!-- header [img] -->
      <img>
          <!-- [alt] attribute -->
        <xsl:attribute name="alt">
          <xsl:value-of select="TEI/text/figure/graphic/desc" />
        </xsl:attribute>
        <xsl:attribute name="class">
          <xsl:value-of select="TEI/text/figure/graphic/@width" />
        </xsl:attribute>
        <xsl:attribute name="src">
          <xsl:value-of select="TEI/text/figure/graphic/@url" />
        </xsl:attribute>
      </img>
    </div>
    <div>
      <xsl:value-of select="TEI/text/note" />
    </div>
  </body>
</html>