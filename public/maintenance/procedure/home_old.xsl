<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:element name="{ypsValens/YPS/@rend}">
      <xsl:element name="{ypsValens/YPS/text/@rend}">
        <xsl:attribute name="{name(ypsValens/YPS/text/@style)}">
          <xsl:value-of select="ypsValens/YPS/text/@style"/>
        </xsl:attribute>
        <xsl:element name="{ypsValens/YPS/text/body/@rend}">
          <xsl:element name="div">
            <xsl:value-of select="ypsValens/YPS/text/body/ab"/>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>