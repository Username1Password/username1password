<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <xsl:element name="{ypsValens/TEI/@rend}">
    <xsl:element name="{ypsValens/TEI/teiHeader/@rend}">
      <xsl:element name="{name(ypsValens/TEI/teiHeader/fileDesc/titleStmt/title)}">
        <xsl:value-of select="ypsValens/TEI/teiHeader/fileDesc/titleStmt/title"/>
      </xsl:element>
    </xsl:element>
    <xsl:element name="{ypsValens/TEI/text/group/text/@rend}">
      <xsl:attribute name="{name(ypsValens/TEI/text/group/text/@style)}">
        <xsl:value-of select="ypsValens/TEI/text/group/text/@style"/>
      </xsl:attribute>
        <!-- TEI front -->
      <xsl:element name="{ypsValens/TEI/text/group/text/front/@rend}">
        <xsl:element name="{ypsValens/TEI/text/group/text/front/head/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/front/head/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/front/head/@style"/>
          </xsl:attribute>
          <xsl:element name="{ypsValens/TEI/text/group/text/front/head/figure/@rend}">
            <xsl:attribute name="{ypsValens/TEI/text/group/text/front/head/figure/figDesc/@rend}">
              <xsl:value-of select="ypsValens/TEI/text/group/text/front/head/figure/figDesc"/>
            </xsl:attribute>
            <xsl:attribute name="{ypsValens/TEI/text/group/text/front/head/figure/graphic/@rend}">
              <xsl:value-of select="ypsValens/TEI/text/group/text/front/head/figure/graphic/@url"/>
            </xsl:attribute>
            <xsl:attribute name="{name(ypsValens/TEI/text/group/text/front/head/figure/graphic/@width)}">
              <xsl:value-of select="ypsValens/TEI/text/group/text/front/head/figure/graphic/@width"/>
            </xsl:attribute>
          </xsl:element>
          <xsl:element name="{ypsValens/TEI/text/group/text/front/head/title/@rend}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/front/head/title" />
          </xsl:element>
        </xsl:element>
        <xsl:element name="{ypsValens/TEI/text/group/text/front/figure/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/front/figure/milestone/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/front/figure/milestone/@style"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="{ypsValens/YPS/text/body/epigraph/@rend}">
          <xsl:element name="{ypsValens/YPS/text/body/epigraph/quote/@rend}">
            <xsl:attribute name="{name(ypsValens/YPS/text/body/epigraph/quote/@style)}">
              <xsl:value-of select="ypsValens/YPS/text/body/epigraph/quote/@style"/>
            </xsl:attribute>
            <xsl:for-each select="ypsValens/YPS/text/body/epigraph/quote/s">
              <xsl:element name="{./@rend}">
                <xsl:choose>
                  <xsl:when test="./seg">
                    <xsl:for-each select="seg">
                      <xsl:choose>
                        <xsl:when test="./ref">
                          <xsl:element name="{./@rend}">
                            <xsl:attribute name="{./ref/@rend}">
                              <xsl:value-of select="./ref/@target" />
                            </xsl:attribute>
                            <xsl:attribute name="{name(./@style)}">
                              <xsl:value-of select="./@style" />
                            </xsl:attribute>
                            <xsl:value-of select="." />
                          </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="." />
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="." />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:element>
            </xsl:for-each>
          </xsl:element>
        </xsl:element>
      </xsl:element>
      <xsl:element name="{ypsValens/TEI/text/group/text/figure/@rend}">
        <xsl:attribute name="{name(ypsValens/TEI/text/group/text/figure/milestone/@style)}">
          <xsl:value-of select="ypsValens/TEI/text/group/text/figure/milestone/@style"/>
        </xsl:attribute>
      </xsl:element>
        <!-- TEI body -->
      <xsl:element name="{ypsValens/TEI/text/group/text/body/@rend}">
        <xsl:element name="{ypsValens/TEI/text/group/text/body/head/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/body/head/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/body/head/@style"/>
          </xsl:attribute>
          <xsl:element name="{ypsValens/TEI/text/group/text/body/head/title/@rend}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/body/head/title" />
          </xsl:element>
        </xsl:element>
        <xsl:element name="{ypsValens/TEI/text/group/text/body/figure/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/body/figure/milestone/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/body/figure/milestone/@style"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="{ypsValens/YPS/text/body/div1/@rend}">
          <xsl:element name="{ypsValens/YPS/text/body/div1/head/@rend}">
            <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/head/@style)}">
              <xsl:value-of select="ypsValens/YPS/text/body/div1/head/@style"/>
            </xsl:attribute>
            <xsl:element name="{ypsValens/YPS/text/body/div1/head/title/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/div1/head/title" />
            </xsl:element>
          </xsl:element>
          <xsl:element name="{ypsValens/YPS/text/body/div1/div2/@rend}">
            <xsl:for-each select="ypsValens/YPS/text/body/div1/div2">
              <xsl:choose>
                <xsl:when test="./@n='1'"/>
                <xsl:otherwise>
                  <xsl:element name="{../figure/@rend}">
                    <xsl:attribute name="{name(../figure/milestone/@style)}">
                      <xsl:value-of select="../figure/milestone/@style"/>
                    </xsl:attribute>
                  </xsl:element>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:element name="{./@rend}">
                <xsl:element name="{./head/title/@rend}">
                  <xsl:attribute name="{name(./head/@style)}">
                    <xsl:value-of select="./head/@style"/>
                  </xsl:attribute>
                  <xsl:value-of select="./head/title" />
                </xsl:element>
                <xsl:for-each select="div3">
                  <xsl:choose>
                    <xsl:when test="./@n='1'"/>
                    <xsl:otherwise>
                      <xsl:element name="{../figure/@rend}">
                        <xsl:attribute name="{name(../figure/milestone/@style)}">
                          <xsl:value-of select="../figure/milestone/@style"/>
                        </xsl:attribute>
                      </xsl:element>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:element name="{./@rend}">
                    <xsl:element name="{./head/title/@rend}">
                      <xsl:attribute name="{name(./head/@style)}">
                        <xsl:value-of select="./head/@style"/>
                      </xsl:attribute>
                      <xsl:value-of select="./head/title" />
                    </xsl:element>
                  </xsl:element>
                  <xsl:element name="{./@rend}">
                    <xsl:for-each select="div4">
                      <xsl:choose>
                        <xsl:when test="./@n='1'"/>
                        <xsl:otherwise>
                          <xsl:element name="{./@rend}">
                            <xsl:element name="{./head/title/@rend}">
                              <xsl:attribute name="{name(./head/@style)}">
                                <xsl:value-of select="./head/@style"/>
                              </xsl:attribute>
                              <xsl:value-of select="./head/title" />
                            </xsl:element>
                          </xsl:element>
                        </xsl:otherwise>
                      </xsl:choose>
                      <xsl:element name="{./@rend}">
                        <xsl:for-each select="div5">
                          <xsl:element name="{./@rend}">
                            <xsl:for-each select="div6">
                              <xsl:element name="{./@rend}">
                                <xsl:for-each select="div7">
                                  <xsl:element name="{./@rend}">
                                    <xsl:for-each select="p">
                                      <xsl:element name="{name()}">
                                        <xsl:attribute name="{name(./@style)}">
                                          <xsl:value-of select="./@style"/>
                                        </xsl:attribute>
                                        <xsl:for-each select="s">
                                          <xsl:element name="{./@rend}">
                                            <xsl:choose>
                                              <xsl:when test="./seg">
                                                <xsl:for-each select="seg">
                                                  <xsl:element name="{./@rend}">
                                                    <xsl:if test="./@style">
                                                      <xsl:attribute name="{name(./@style)}">
                                                        <xsl:value-of select="./@style" />
                                                      </xsl:attribute>
                                                    </xsl:if>
                                                    <xsl:if test="./ref">
                                                      <xsl:attribute name="{./ref/@rend}">
                                                        <xsl:value-of select="./ref/@target" />
                                                      </xsl:attribute>
                                                    </xsl:if>
                                                    <xsl:value-of select="." />
                                                  </xsl:element>
                                                </xsl:for-each>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:value-of select="." />
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:element>
                                        </xsl:for-each>
                                      </xsl:element>
                                    </xsl:for-each>
                                  </xsl:element>
                                </xsl:for-each>
                              </xsl:element>
                            </xsl:for-each>
                          </xsl:element>
                        </xsl:for-each>
                      </xsl:element>
                    </xsl:for-each>
                  </xsl:element>
                </xsl:for-each>
              </xsl:element>
            </xsl:for-each>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
