<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
    <body style="background-color: #ffefd5">
      <div>
        <div>
          <xsl:value-of select="TEI/text/body/head/note" />
        </div>
        <br />
        <div>
          <div>
            <xsl:value-of select="TEI/text/body/head/figure/head" />
          </div>
          <br />
          <div>
            <xsl:value-of select="TEI/text/body/head/figure/figDesc" />
          </div>
          <br />
          <div>
            <div>
              <xsl:value-of select="TEI/text/body/head/figure/bibl-n1/note" />
            </div>
            <br />
            <div>
              <div>
                <xsl:value-of select="TEI/text/body/head/figure/bibl-n1/availability/license" />
              </div>
            </div>
          </div>
          <br />
          <div>
            <div>
              <div>
                <xsl:value-of select="TEI/text/body/head/figure/bibl-n2/availability/license" />
              </div>
            </div>
          </div>
        </div>
        <br />
        <div>
          <xsl:value-of select="TEI/text/body/head/title" />
        </div>
      </div>
      <br />
      <div>
        <div>
          <xsl:value-of select="TEI/text/body/div1/head" />
        </div>
        <br />
        <div>
          <div>
            <xsl:value-of select="TEI/text/body/div1/div2/head" />
          </div>
          <br />
          <div>
            <div>
              <xsl:value-of select="TEI/text/body/div1/div2/div3/head" />
            </div>
            <br />
            <div>
              <div>
                <xsl:value-of select="TEI/text/body/div1/div2/div3/div4/head" />
              </div>
              <br />
              <div>
                <div>
                  <xsl:value-of select="TEI/text/body/div1/div2/div3/div4/div5/head" />
                </div>
                <br />
                <div>
                  <xsl:value-of select="TEI/text/body/div1/div2/div3/div4/div5/p-n1" />
                </div>
                <br />
                <div>
                  <xsl:value-of select="TEI/text/body/div1/div2/div3/div4/div5/p-n2" />
                </div>
                <br />
                <div>
                  <div>
                    <xsl:value-of select="TEI/text/body/div1/div2/div3/div4/div5/u/unclear" />
                  </div>
                </div>
                <br />
                <div>
                  <xsl:value-of select="TEI/text/body/div1/div2/div3/div4/div5/trailer" />
                </div>
              </div>
            </div>
          </div>
          <br />
          <div>
            <xsl:value-of select="TEI/text/body/div1/div2/trailer" />
          </div>
        </div>
        <br />
        <div>
          <xsl:value-of select="TEI/text/body/div1/trailer" />
        </div>
      </div>
      <br />
      <div>
        <xsl:value-of select="TEI/text/body/trailer" />
      </div>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
