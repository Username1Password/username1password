<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <!-- html -->
  <xsl:element name="{ypsValens/teiCorpus/TEI[2]/@rend}">
      <!-- head -->
    <xsl:element name="{ypsValens/teiCorpus/TEI[2]/teiHeader/@rend}">
        <!-- title -->
      <xsl:element name="{name(ypsValens/teiCorpus/teiHeader/fileDesc/titleStmt/title)}">
        <xsl:value-of select="ypsValens/teiCorpus/teiHeader/fileDesc/titleStmt/title"/>
      </xsl:element>
    </xsl:element>
      <!-- body -->
    <xsl:element name="{ypsValens/teiCorpus/TEI[2]/text/group/text/@rend}">
        <!-- body style (margin, color, font) -->
      <xsl:attribute name="{name(ypsValens/teiCorpus/TEI[2]/text/group/text/@style)}">
        <xsl:value-of select="ypsValens/teiCorpus/TEI[2]/text/group/text/@style"/>
      </xsl:attribute>
    </xsl:element>
      <!-- div -->
    <xsl:element name="{ypsValens/teiCorpus/TEI[2]/text/front/@rend}">
        <!-- div style (text center) -->
      <xsl:attribute name="{name(ypsValens/teiCorpus/TEI[2]/text/front/@style)}">
        <xsl:value-of select="ypsValens/teiCorpus/TEI[2]/text/front/@style"/>
      </xsl:attribute>
        <!-- img -->
      <xsl:element name="{ypsValens/teiCorpus/TEI[2]/text/front/figure/@rend}">
          <!-- img alt -->
        <xsl:attribute name="{ypsValens/teiCorpus/TEI[2]/text/front/figure/figDesc/@rend}">
          <xsl:value-of select="ypsValens/teiCorpus/TEI[2]/text/front/figure/figDesc"/>
        </xsl:attribute>
          <!-- img src -->
        <xsl:attribute name="{ypsValens/teiCorpus/TEI[2]/text/front/figure/graphic/@rend}">
          <xsl:value-of select="ypsValens/teiCorpus/TEI[2]/text/front/figure/graphic/@url"/>
        </xsl:attribute>
          <!-- img width -->
        <xsl:attribute name="{name(ypsValens/teiCorpus/TEI[2]/text/front/figure/graphic/@width)}">
          <xsl:value-of select="ypsValens/teiCorpus/TEI[2]/text/front/figure/graphic/@width"/>
        </xsl:attribute>
      </xsl:element>
        <!-- title -->
      <xsl:element name="{ypsValens/teiCorpus/TEI[2]/text/front/head/title/@rend}">
        <xsl:value-of select="ypsValens/teiCorpus/TEI[2]/text/front/head/title" />
      </xsl:element>
    </xsl:element>
      <!-- hr -->
    <xsl:element name="{ypsValens/teiCorpus/TEI[2]/text/figure/@rend}">
        <!-- hr (72.1%) -->
      <xsl:attribute name="{name(ypsValens/teiCorpus/TEI[2]/text/figure/milestone/@style)}">
        <xsl:value-of select="ypsValens/teiCorpus/TEI[2]/text/figure/milestone/@style"/>
      </xsl:attribute>
    </xsl:element>
      <!-- [epigraph] blockquote -->
    <xsl:element name="{ypsValens/YPS/text/group/text/front/epigraph/quote/@rend}">
        <!-- blockquote style (text justify) -->
      <xsl:attribute name="{name(ypsValens/YPS/text/group/text/front/epigraph/quote/@style)}">
        <xsl:value-of select="ypsValens/YPS/text/group/text/front/epigraph/quote/@style"/>
      </xsl:attribute>
      <xsl:for-each select="ypsValens/YPS/text/group/text/front/epigraph/quote/s">
        <xsl:element name="{./@rend}">
          <xsl:choose>
            <xsl:when test="./seg">
              <xsl:for-each select="seg">
                <xsl:choose>
                  <xsl:when test="./ref">
                    <xsl:element name="{./@rend}">
                      <xsl:attribute name="{./ref/@rend}">
                        <xsl:value-of select="./ref/@target" />
                      </xsl:attribute>
                      <xsl:attribute name="{name(./@style)}">
                        <xsl:value-of select="./@style" />
                      </xsl:attribute>
                      <xsl:value-of select="." />
                    </xsl:element>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="." />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="." />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
