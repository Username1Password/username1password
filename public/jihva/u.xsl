<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:element name="{ypsValens/YPS/@rend}">
      <xsl:element name="{ypsValens/YPS/ypsHeader/@rend}">
        <xsl:element name="{ypsValens/YPS/ypsHeader/encodingDesc/styleDefDecl/ab/@rend}">
          <xsl:attribute name="{ypsValens/YPS/ypsHeader/encodingDesc/styleDefDecl/ab/ref/@rend}">
            <xsl:value-of select="ypsValens/YPS/ypsHeader/encodingDesc/styleDefDecl/ab/ref/@target"/>
          </xsl:attribute>
          <xsl:attribute name="{ypsValens/YPS/ypsHeader/encodingDesc/styleDefDecl/ab/ref/att/@rend}">
            <xsl:value-of select="ypsValens/YPS/ypsHeader/encodingDesc/styleDefDecl/ab/ref/att"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:element>
      <xsl:element name="{ypsValens/YPS/text/@rend}">
        <xsl:element name="{ypsValens/YPS/text/body/@rend}">
          <xsl:element name="{ypsValens/YPS/text/body/said/@rend}">
            <xsl:attribute name="{ypsValens/YPS/text/body/said/elementSpec/@ident}">
              <xsl:value-of select="ypsValens/YPS/text/body/said/elementSpec/model/@cssClass"/>
            </xsl:attribute>
            <xsl:element name="{ypsValens/YPS/text/body/said/temp/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/said/temp"/>
            </xsl:element>
            <xsl:element name="{ypsValens/YPS/text/body/said/unclear/@rend}">
              <xsl:attribute name="{ypsValens/YPS/text/body/said/unclear/elementSpec/@ident}">
                <xsl:value-of select="ypsValens/YPS/text/body/said/unclear/elementSpec/model/@cssClass"/>
              </xsl:attribute>
              <xsl:value-of select="ypsValens/YPS/text/body/said/unclear"/>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
