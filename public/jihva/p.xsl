<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:element name="{ypsValens/YPS/@rend}">
      <xsl:element name="{ypsValens/YPS/text/@rend}">
        <xsl:attribute name="{name(ypsValens/YPS/text/@style)}">
          <xsl:value-of select="ypsValens/YPS/text/@style"/>
        </xsl:attribute>
        <xsl:element name="{ypsValens/YPS/text/body/@rend}">
          <xsl:element name="div">
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[1]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[1]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[2]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[2]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[3]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[3]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[4]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[4]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[5]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[5]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[6]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[6]"/>
            </xsl:element>
            <br /><br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[7]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[7]"/>
            </xsl:element>
            <br /><br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[8]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[8]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[9]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[9]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[10]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[10]"/>
            </xsl:element>
            <br /><br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[11]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[11]"/>
            </xsl:element>
            <br /><br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[12]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[12]"/>
            </xsl:element>
            <br /><br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[13]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[13]"/>
            </xsl:element>
            <br /><br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[14]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[14]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[15]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[15]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[16]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[16]"/>
            </xsl:element>
            <br />
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg[17]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg[17]"/>
            </xsl:element>
          </xsl:element>
          <xsl:element name="{ypsValens/YPS/text/body/ab[2]/@rend}">
            <xsl:attribute name="style">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/@style"/>
            </xsl:attribute>
            <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[1]/@rend}">
              <xsl:attribute name="style">
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[1]/@style"/>
              </xsl:attribute>
              <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[1]"/>
            </xsl:element>
            <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[2]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[2]"/>
            </xsl:element>
            <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[3]/@rend}">
              <xsl:attribute name="style">
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[3]/@style"/>
              </xsl:attribute>
              <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[3]"/>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
