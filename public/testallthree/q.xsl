<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:element name="{ypsValens/YPS/@rend}">
      <xsl:element name="{ypsValens/YPS/text/@rend}">
        <xsl:attribute name="{name(ypsValens/YPS/text/@style)}">
          <xsl:value-of select="ypsValens/YPS/text/@style"/>
        </xsl:attribute>
        <xsl:element name="{ypsValens/YPS/text/body/@rend}">
          <xsl:element name="div">
            <xsl:element name="{ypsValens/YPS/text/body/ab[1]/seg/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[1]/seg"/>
            </xsl:element>
          </xsl:element>
          <xsl:element name="{ypsValens/YPS/text/body/ab[2]/@rend}">
            <xsl:attribute name="style">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/@style"/>
            </xsl:attribute>
            <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[1]/@rend}">
              <xsl:attribute name="style">
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[1]/@style"/>
              </xsl:attribute>
              <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[1]"/>
            </xsl:element>
            <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[2]/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[2]"/>
            </xsl:element>
            <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[3]/@rend}">
              <xsl:attribute name="style">
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[3]/@style"/>
              </xsl:attribute>
              <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[3]"/>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
