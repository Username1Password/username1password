<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:element name="{one/two/@rend}">
      <xsl:element name="{one/two/three/@rend}">
        <xsl:element name="{one/two/three/four/@rend}">
          <xsl:value-of select="one/two/three/four"/>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>